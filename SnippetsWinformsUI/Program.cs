﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright company="ChrisDugdale" file="Program.cs">
// ChrisDugdale
// </copyright>
// <summary>
//   Class with program entry point.
// </summary>
// 
// --------------------------------------------------------------------------------------------------------------------
namespace SnippetsWinformsUI
{
    using System;
    using System.Windows.Forms;

    /// <summary>
    /// Class with program entry point.
    /// </summary>
    internal sealed class Program
    {
        #region Methods

        /// <summary>
        /// Program entry point.
        /// </summary>
        /// <param name="args">
        /// The args.
        /// </param>
        [STAThread]
        private static void Main(string[] args)
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new MainForm());
        }

        #endregion
    }
}