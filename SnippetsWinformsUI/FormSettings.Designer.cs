﻿namespace SnippetsWinformsUI
{
    partial class FormSettings
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.chkShowHiddenFolders = new System.Windows.Forms.CheckBox();
            this.btnSaveChanges = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.chkConfirmExit = new System.Windows.Forms.CheckBox();
            this.chkSaveInitialDirectory = new System.Windows.Forms.CheckBox();
            this.txtFilePath = new System.Windows.Forms.TextBox();
            this.btnBrowse = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnChooseFileExtensions = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // chkShowHiddenFolders
            // 
            this.chkShowHiddenFolders.AutoSize = true;
            this.chkShowHiddenFolders.Location = new System.Drawing.Point(13, 13);
            this.chkShowHiddenFolders.Name = "chkShowHiddenFolders";
            this.chkShowHiddenFolders.Size = new System.Drawing.Size(167, 17);
            this.chkShowHiddenFolders.TabIndex = 0;
            this.chkShowHiddenFolders.Text = "Show hidden files and folders ";
            this.chkShowHiddenFolders.UseVisualStyleBackColor = true;
            // 
            // btnSaveChanges
            // 
            this.btnSaveChanges.Location = new System.Drawing.Point(106, 422);
            this.btnSaveChanges.Name = "btnSaveChanges";
            this.btnSaveChanges.Size = new System.Drawing.Size(75, 23);
            this.btnSaveChanges.TabIndex = 1;
            this.btnSaveChanges.Text = "Save";
            this.btnSaveChanges.UseVisualStyleBackColor = true;
            this.btnSaveChanges.Click += new System.EventHandler(this.BtnSaveChangesClick);
            // 
            // btnCancel
            // 
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(187, 422);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 2;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.BtnCancelClick);
            // 
            // chkConfirmExit
            // 
            this.chkConfirmExit.AutoSize = true;
            this.chkConfirmExit.Location = new System.Drawing.Point(13, 36);
            this.chkConfirmExit.Name = "chkConfirmExit";
            this.chkConfirmExit.Size = new System.Drawing.Size(95, 17);
            this.chkConfirmExit.TabIndex = 3;
            this.chkConfirmExit.Text = "Confirm on exit";
            this.chkConfirmExit.UseVisualStyleBackColor = true;
            // 
            // chkSaveInitialDirectory
            // 
            this.chkSaveInitialDirectory.AutoSize = true;
            this.chkSaveInitialDirectory.Location = new System.Drawing.Point(6, 9);
            this.chkSaveInitialDirectory.Name = "chkSaveInitialDirectory";
            this.chkSaveInitialDirectory.Size = new System.Drawing.Size(111, 17);
            this.chkSaveInitialDirectory.TabIndex = 4;
            this.chkSaveInitialDirectory.Text = "Set Initial Directoy";
            this.chkSaveInitialDirectory.UseVisualStyleBackColor = true;
            this.chkSaveInitialDirectory.CheckedChanged += new System.EventHandler(this.ChkSaveInitialDirectoryCheckedChanged);
            // 
            // txtFilePath
            // 
            this.txtFilePath.Location = new System.Drawing.Point(6, 32);
            this.txtFilePath.Name = "txtFilePath";
            this.txtFilePath.Size = new System.Drawing.Size(282, 20);
            this.txtFilePath.TabIndex = 11;
            // 
            // btnBrowse
            // 
            this.btnBrowse.Location = new System.Drawing.Point(294, 30);
            this.btnBrowse.Name = "btnBrowse";
            this.btnBrowse.Size = new System.Drawing.Size(75, 23);
            this.btnBrowse.TabIndex = 12;
            this.btnBrowse.Text = "Browse...";
            this.btnBrowse.UseVisualStyleBackColor = true;
            this.btnBrowse.Click += new System.EventHandler(this.BtnBrowseClick);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.chkSaveInitialDirectory);
            this.groupBox1.Controls.Add(this.btnBrowse);
            this.groupBox1.Controls.Add(this.txtFilePath);
            this.groupBox1.Location = new System.Drawing.Point(13, 114);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(369, 62);
            this.groupBox1.TabIndex = 13;
            this.groupBox1.TabStop = false;
            // 
            // btnChooseFileExtensions
            // 
            this.btnChooseFileExtensions.Location = new System.Drawing.Point(19, 182);
            this.btnChooseFileExtensions.Name = "btnChooseFileExtensions";
            this.btnChooseFileExtensions.Size = new System.Drawing.Size(138, 23);
            this.btnChooseFileExtensions.TabIndex = 14;
            this.btnChooseFileExtensions.Text = "Chose file extensions...";
            this.btnChooseFileExtensions.UseVisualStyleBackColor = true;
            this.btnChooseFileExtensions.Click += new System.EventHandler(this.BtnChooseFileExtensionsClick);
            // 
            // FormSettings
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(393, 457);
            this.Controls.Add(this.btnChooseFileExtensions);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.chkConfirmExit);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnSaveChanges);
            this.Controls.Add(this.chkShowHiddenFolders);
            this.MinimumSize = new System.Drawing.Size(409, 495);
            this.Name = "FormSettings";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Options";
            this.TopMost = true;
            this.Load += new System.EventHandler(this.FormSettingsLoad);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.CheckBox chkShowHiddenFolders;
        private System.Windows.Forms.Button btnSaveChanges;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.CheckBox chkConfirmExit;
        private System.Windows.Forms.CheckBox chkSaveInitialDirectory;
        private System.Windows.Forms.TextBox txtFilePath;
        private System.Windows.Forms.Button btnBrowse;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnChooseFileExtensions;
    }
}