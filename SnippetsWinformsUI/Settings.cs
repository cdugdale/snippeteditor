﻿namespace SnippetsWinformsUI
{
    using System.ComponentModel;
    using System.Configuration;

    // This class allows you to handle specific events on the settings class:
    // The SettingChanging event is raised before a setting's value is changed.
    // The PropertyChanged event is raised after a setting's value is changed.
    // The SettingsLoaded event is raised after the setting values are loaded.
    // The SettingsSaving event is raised before the setting values are saved.
    
    /// <summary>The settings.</summary>
    internal sealed partial class Settings
    {
        #region Constructors and Destructors

        /// <summary>Initialises a new instance of the <see cref="Settings"/> class.</summary>
        public Settings()
        {
            // todo: stuff here:// // To add event handlers for saving and changing settings, uncomment the lines below:
            // this.SettingChanging += this.SettingChangingEventHandler;
             this.SettingsSaving += this.SettingsSavingEventHandler;
        }



        #endregion

        #region Methods

        /// <summary>The setting changing event handler.</summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The e.</param>
        private void SettingChangingEventHandler(object sender, SettingChangingEventArgs e)
        {
            // Add code to handle the SettingChangingEvent event here.
        }

        /// <summary>The settings saving event handler.</summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The e.</param>
        private void SettingsSavingEventHandler(object sender, CancelEventArgs e)
        {
            // Add code to handle the SettingsSaving event here.
        }

        #endregion
    }
}

/*
  this.radSplitContainer1.SplitPanels["splitPanel1"].SizeInfo.SizeMode = Telerik.WinControls.UI.Docking.SplitPanelSizeMode.Absolute; 
this.radSplitContainer1.SplitPanels["splitPanel1"].SizeInfo.AbsoluteSize = new Size(150, 0); 
 * 
 * 
 * 
 * 
 * 
 * * namespace Settings
{
    public partial class UserSettingsDemo : Form
    {
        string ConnectionString = string.Empty;
        public UserSettingsDemo()
        {
            InitializeComponent();

        }

        private void button1_Click(object sender, EventArgs e)
        {
            txtOuput.Text = "Ouput goes here...................";
        }

        private void chkIntegratedSecurity_CheckedChanged(object sender, EventArgs e)
        {
            if (chkIntegratedSecurity.Checked)
            {
                txtUserId.Enabled = false;
                txtPassword.Enabled = false;
            }
        }

        private void btnTestConnection_Click(object sender, EventArgs e)
        {
            if(TestConnection())
            txtInput.Enabled = true;
        }

        private bool TestConnection()
        {
            return true;
        }

        private void UserSettingsDemo_Load(object sender, EventArgs e)
        {
            txtServer.Text = Settings.Default.ServerNameSetting;
            txtDatabase.Text = Settings.Default.DBNameSetting;
            txtPassword.Text = Settings.Default.PasswordSetting;
            txtUserId.Text = Settings.Default.UserIdSetting;
        }
        
        private void UserSettingsDemo_FormClosing(object sender, FormClosingEventArgs e)
        {
            Settings.Default.DBNameSetting = txtDatabase.Text;
            Settings.Default.UserIdSetting = txtUserId.Text;
            Settings.Default.PasswordSetting = txtPassword.Text;
            Settings.Default.ServerNameSetting = txtServer.Text;
            Settings.Default.Save();

        }
     */

/*
 * public Settings() {
            // // To add event handlers for saving and changing settings, uncomment the lines below:
            //
            // this.SettingChanging += this.SettingChangingEventHandler;
            //
            // this.SettingsSaving += this.SettingsSavingEventHandler;

*/