﻿namespace SnippetsWinformsUI
{
    using System;
    using System.Windows.Forms;

    /// <summary>
    ///   The form settings.
    /// </summary>
    public partial class FormSettings : Form
    {
        #region Constructors and Destructors

        /// <summary>
        ///   Initialises a new instance of the <see cref="FormSettings" /> class.
        /// </summary>
        public FormSettings()
        {
            this.InitializeComponent();
        }

        #endregion

        #region Methods

        /// <summary>The btn browse click.</summary>
        /// <param name="sender">The sender. </param>
        /// <param name="e">The e. </param>
        private void BtnBrowseClick(object sender, EventArgs e)
        {
            using (new CenterWinDialog(this))
            {
                var dialog = new FolderBrowserDialog();

                if (dialog.ShowDialog() == DialogResult.OK)
                {
                    Settings.Default.InitialDirectorySetting = dialog.SelectedPath;
                }
            }

            this.txtFilePath.Text = Settings.Default.InitialDirectorySetting;
        }

        /// <summary>The button cancel click.</summary>
        /// <param name="sender">The sender. </param>
        /// <param name="e">The e. </param>
        private void BtnCancelClick(object sender, EventArgs e)
        {
            // this.RaiseCancelButtonPressed(e);
            this.Close();
        }

        /// <summary>The button choose file extensions click.</summary>
        /// <param name="sender">The sender. </param>
        /// <param name="e">The e. </param>
        private void BtnChooseFileExtensionsClick(object sender, EventArgs e)
        {
            using (new CenterWinDialog(this))
            {
                var dialog = new FormFileExtensions();
                dialog.ShowDialog(this);
            }
        }

        /// <summary>Saves changes to the settings and then closes the form.</summary>
        /// <param name="sender">The sender. </param>
        /// <param name="e">The e. </param>
        private void BtnSaveChangesClick(object sender, EventArgs e)
        {
            Settings.Default.ShowHIddenFilesAndFoldersSetting = this.chkShowHiddenFolders.Checked;
            Settings.Default.ConfirmExitSetting = this.chkConfirmExit.Checked;
            Settings.Default.SaveInitialDirectorySetting = this.chkSaveInitialDirectory.Checked;
            Settings.Default.InitialDirectorySetting = this.txtFilePath.Text;
            Settings.Default.Save();

            // this.RaiseSaveButtonPressed(e);
            this.Close();
        }

        /// <summary>The check save initial directory checked changed.</summary>
        /// <param name="sender">The sender. </param>
        /// <param name="e">The e. </param>
        private void ChkSaveInitialDirectoryCheckedChanged(object sender, EventArgs e)
        {
            var checkbox = (CheckBox)sender;

            if (checkbox.Checked)
            {
                this.txtFilePath.Enabled = true;
                this.btnBrowse.Enabled = true;
            }
            else
            {
                this.txtFilePath.Enabled = false;
                this.btnBrowse.Enabled = false;
            }
        }

        /// <summary>The form settings load.</summary>
        /// <param name="sender">The sender. </param>
        /// <param name="e">The e. </param>
        private void FormSettingsLoad(object sender, EventArgs e)
        {
            this.chkShowHiddenFolders.Checked = Settings.Default.ShowHIddenFilesAndFoldersSetting;
            this.chkConfirmExit.Checked = Settings.Default.ConfirmExitSetting;

            this.chkSaveInitialDirectory.Checked = Settings.Default.SaveInitialDirectorySetting;
            this.btnBrowse.Enabled = Settings.Default.SaveInitialDirectorySetting;
            this.txtFilePath.Enabled = Settings.Default.SaveInitialDirectorySetting;
            this.txtFilePath.Text = Settings.Default.InitialDirectorySetting;
        }

        #endregion
    }
}