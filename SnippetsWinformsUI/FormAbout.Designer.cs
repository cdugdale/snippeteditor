﻿using System;
namespace SnippetsWinformsUI
{
	partial class FormAbout
	{
		/// <summary>
		/// Designer variable used to keep track of non-visual components.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		
		/// <summary>
		/// Disposes resources used by the form.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing) {
				if (components != null) {
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
		
		/// <summary>
		/// This method is required for Windows Forms designer support.
		/// Do not change the method contents inside the source code editor. The Forms designer might
		/// not be able to load this method if it was changed manually.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormAbout));
            this.LinkLicence = new System.Windows.Forms.LinkLabel();
            this.linkGitHub = new System.Windows.Forms.LinkLabel();
            this.buttonClose = new System.Windows.Forms.Button();
            this.labelBlurb = new System.Windows.Forms.Label();
            this.labelVersion = new System.Windows.Forms.Label();
            this.buttonChangeLog = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // LinkLicence
            // 
            this.LinkLicence.LinkBehavior = System.Windows.Forms.LinkBehavior.HoverUnderline;
            this.LinkLicence.Location = new System.Drawing.Point(0, 236);
            this.LinkLicence.Name = "LinkLicence";
            this.LinkLicence.Size = new System.Drawing.Size(300, 18);
            this.LinkLicence.TabIndex = 0;
            this.LinkLicence.TabStop = true;
            this.LinkLicence.Text = "link to licence";
            this.LinkLicence.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.LinkLicence.VisitedLinkColor = System.Drawing.Color.Blue;
            this.LinkLicence.MouseClick += new System.Windows.Forms.MouseEventHandler(this.LinkLicenceMouseClick);
            // 
            // linkGitHub
            // 
            this.linkGitHub.LinkBehavior = System.Windows.Forms.LinkBehavior.HoverUnderline;
            this.linkGitHub.Location = new System.Drawing.Point(0, 193);
            this.linkGitHub.Name = "linkGitHub";
            this.linkGitHub.Size = new System.Drawing.Size(300, 18);
            this.linkGitHub.TabIndex = 1;
            this.linkGitHub.TabStop = true;
            this.linkGitHub.Text = "link to GitHub or SourceForge?";
            this.linkGitHub.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.linkGitHub.VisitedLinkColor = System.Drawing.Color.Blue;
            this.linkGitHub.MouseClick += new System.Windows.Forms.MouseEventHandler(this.LinkGitHubMouseClick);
            // 
            // buttonClose
            // 
            this.buttonClose.FlatAppearance.BorderColor = System.Drawing.SystemColors.GrayText;
            this.buttonClose.Location = new System.Drawing.Point(152, 12);
            this.buttonClose.Name = "buttonClose";
            this.buttonClose.Size = new System.Drawing.Size(75, 23);
            this.buttonClose.TabIndex = 3;
            this.buttonClose.Text = "Close";
            this.buttonClose.UseVisualStyleBackColor = true;
            this.buttonClose.Click += new System.EventHandler(this.ButtonCloseClick);
            // 
            // labelBlurb
            // 
            this.labelBlurb.Location = new System.Drawing.Point(0, 86);
            this.labelBlurb.Name = "labelBlurb";
            this.labelBlurb.Padding = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.labelBlurb.Size = new System.Drawing.Size(300, 39);
            this.labelBlurb.TabIndex = 4;
            this.labelBlurb.Text = resources.GetString("labelBlurb.Text");
            // 
            // labelVersion
            // 
            this.labelVersion.Location = new System.Drawing.Point(0, 150);
            this.labelVersion.Name = "labelVersion";
            this.labelVersion.Size = new System.Drawing.Size(300, 18);
            this.labelVersion.TabIndex = 5;
            this.labelVersion.Text = "Version populates here";
            this.labelVersion.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // buttonChangeLog
            // 
            this.buttonChangeLog.Location = new System.Drawing.Point(71, 12);
            this.buttonChangeLog.Name = "buttonChangeLog";
            this.buttonChangeLog.Size = new System.Drawing.Size(75, 23);
            this.buttonChangeLog.TabIndex = 6;
            this.buttonChangeLog.Text = "Change Log";
            this.buttonChangeLog.UseVisualStyleBackColor = true;
            this.buttonChangeLog.Click += new System.EventHandler(this.ButtonChangeLogClick);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.Control;
            this.panel1.Controls.Add(this.buttonClose);
            this.panel1.Controls.Add(this.buttonChangeLog);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 271);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(300, 47);
            this.panel1.TabIndex = 7;
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(0, 125);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(300, 25);
            this.label2.TabIndex = 9;
            this.label2.Text = "Version";
            this.label2.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            // 
            // label3
            // 
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(0, 211);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(300, 25);
            this.label3.TabIndex = 10;
            this.label3.Text = "Licence";
            this.label3.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            // 
            // label4
            // 
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(0, 168);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(300, 25);
            this.label4.TabIndex = 11;
            this.label4.Text = "Source";
            this.label4.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            // 
            // label5
            // 
            this.label5.Image = ((System.Drawing.Image)(resources.GetObject("label5.Image")));
            this.label5.Location = new System.Drawing.Point(0, 9);
            this.label5.Name = "label5";
            this.label5.Padding = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label5.Size = new System.Drawing.Size(300, 62);
            this.label5.TabIndex = 12;
            // 
            // FormAbout
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.ClientSize = new System.Drawing.Size(300, 318);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.linkGitHub);
            this.Controls.Add(this.LinkLicence);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.labelVersion);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.labelBlurb);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "FormAbout";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Snippet Editor - About";
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

		}
		private System.Windows.Forms.Label labelBlurb;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.Button buttonChangeLog;
		private System.Windows.Forms.Label labelVersion;
		private System.Windows.Forms.Button buttonClose;
		private System.Windows.Forms.LinkLabel linkGitHub;
		private System.Windows.Forms.LinkLabel LinkLicence;
	}
}
