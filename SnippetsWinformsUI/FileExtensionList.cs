﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SnippetsWinformsUI
{
    /// <summary>The file extension list.</summary>
    internal class FileExtensionList
    {
        /// <summary>The _checked state.</summary>
        private bool checkedState;

        /// <summary>The _file extension.</summary>
        private string fileExtension;

        /// <summary>Initialises a new instance of the <see cref="FileExtensionList"/> class.</summary>
        public FileExtensionList()
        {

        }

        /// <summary>Gets or sets a value indicating whether checked state.</summary>
        public bool CheckedState
        {
            get
            {
                return this.checkedState;
            }
            set
            {
                this.checkedState = value;
            }
        }

        /// <summary>Gets or sets the file extension.</summary>
        public string FileExtension
        {
            get
            {
                return this.fileExtension;
            }
            set
            {
                this.fileExtension = value;
            }
        }
    }
}