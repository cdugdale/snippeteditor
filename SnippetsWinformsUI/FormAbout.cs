﻿namespace SnippetsWinformsUI
{
    using System;
    using System.Windows.Forms;

    /// <summary> Description of FormAbout. </summary>
    public partial class FormAbout : Form
    {
        #region Constructors and Destructors

        /// <summary>Initialises a new instance of the <see cref="FormAbout"/> class.</summary>
        public FormAbout()
        {
            // The InitializeComponent() call is required for Windows Forms designer support.
            this.InitializeComponent();
            this.labelVersion.Text = Application.ProductVersion.TrimEndOfVersionNumber();

            // Add constructor code after the InitializeComponent() call.
        }

        #endregion

        #region Methods

        /// <summary> Opens the FormChangeLog showing ChangeLog.html </summary>
        /// <param name="sender"> button the button that sent the click event </param>
        /// <param name="e"> EventArgs event arguments </param>
        private void ButtonChangeLogClick(object sender, EventArgs e)
        {
            using (new CenterWinDialog(this))
            {
                var changeLog = new FormChangeLog();
                changeLog.ShowDialog(this);
            }
        }

        /// <summary> closes the about form, not the whole app </summary>
        /// <param name="sender"> button the button that sent the click event </param>
        /// <param name="e"> EventArgs event arguments </param>
        private void ButtonCloseClick(object sender, EventArgs e)
        {
            this.Close();
        }

        #endregion

        /// <summary>The link to online source control mouse click.</summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The e.</param>
        private void LinkGitHubMouseClick(object sender, MouseEventArgs e)
        {
            // implement the link to the sourcecode wherever i put that!
        }

        /// <summary>The link to the terms of use mouse click.</summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The e.</param>
        private void LinkLicenceMouseClick(object sender, MouseEventArgs e)
        {
            // todo: implement the licence link
        }
    }
}