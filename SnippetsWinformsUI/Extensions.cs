﻿namespace SnippetsWinformsUI
{
    /// <summary>The extensions.</summary>
    public static class Extensions
    {
        #region Public Methods and Operators

        /// <summary>The trim end of version number method takes the trailing zero and period from the version number in the assembly info file.</summary>
        /// <param name="str">The string to trim.</param>
        /// <returns>The <see cref="string"/>.</returns>
        public static string TrimEndOfVersionNumber(this string str)
        {
            return string.IsNullOrEmpty(str) ? str : str.TrimEnd('.', '0');
        }

        /// <summary>The trim last char.</summary>
        /// <param name="str">The string to trim.</param>
        /// <returns>The <see cref="string"/>.</returns>
        public static string TrimLastChar(this string str)
        {
            return string.IsNullOrEmpty(str) ? str : str.TrimEnd(str[str.Length - 1]);
        }

        #endregion
    }
}