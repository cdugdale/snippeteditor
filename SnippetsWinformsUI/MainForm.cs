﻿#region TODO

/*TODO: Improvements/ fixes:
 * Check for security settings on treeview navigate for files to open
 * Add folder creation and folder delete
 * Add null reference check into treeview refresh that takes the file as the parameter.
 * make the cursor go back to the place that it was editing, this could be hard as the file is completely reloaded. 
 * make the mainform return to the centre of the screen, or at least the border if any of the settings push it off the screen. 
 * Find a list of the supported file types for the rich textbox
 * Introduce an object to store what is now global variables, openFile object that stores text changed, path, last saved time etc.
*/
#endregion

namespace SnippetsWinformsUI
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Drawing;
    using System.IO;
    using System.Windows.Forms;

    /// <summary>
    ///   The main form that starts when the project is run.
    /// </summary>
    public partial class MainForm : Form
    {
        #region Fields

        /// <summary>
        ///   Holds the entire path of the open file
        /// </summary>
        private string fileAndPathOfOpenFile = string.Empty;

        /// <summary>
        ///   Set to true if UI is refreshing to bypass normal check for changes on changing selection.
        /// </summary>
        private bool isOnRefresh;

        /// <summary>
        ///   Holds the file name of the last selected item from the list view
        /// </summary>
        private string lastSelectedListBoxItem = string.Empty;

        /// <summary>
        ///   The last selected node.
        /// </summary>
        private TreeNode lastSelectedNode = new TreeNode();

        /// <summary>
        ///   The last selected node full path.
        /// </summary>
        private string lastSelectedNodeFullPath = string.Empty;

        /// <summary>
        ///   This variable has been introduced to keep track of the number of times the process has gone through the list box selected item changed as the unsaved changes prompt was appearing twice.
        /// </summary>
        private bool listBoxFirstPass = true;

        /// <summary>
        ///   The saved file contents, saves the entire value of the text file when it is loaded.
        /// </summary>
        private string savedFileContents = string.Empty;

        /// <summary>
        ///   This is updated each time the textbox text is changed. allows check for changes since saved.
        /// </summary>
        private string textBoxCurrentContents = string.Empty;

        /// <summary>
        ///   This variable has been introduced to keep track of the number of times the process has gone through the TreeView select as the unsaved changes prompt was appearing twice.
        /// </summary>
        private bool treeViewFirstPass = true;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        ///   Initialises a new instance of the <see cref="MainForm" /> class.
        /// </summary>
        public MainForm()
        {
            try
            {
                this.InitializeComponent();

                this.Width = Settings.Default.MainWindowWidthSetting;
                this.Height = Settings.Default.MainWindowHeightSetting;
                this.Location = Settings.Default.MainWindowLocationSetting;
                this.Size = Settings.Default.MainWindowSizeSetting;
                
                this.TreeviewPopulate();
                this.treeViewExplorer.Nodes[0].EnsureVisible();

                if (Settings.Default.SaveInitialDirectorySetting)
                {
                    this.RefreshTreeView(Settings.Default.InitialDirectorySetting);
                }
                
                // Debug code below tries to put focus on the Treeview and to test what it is doing, it jsut won't work!
                // this.splitContainerTreeviewListbox.Panel2.Controls[0].Focus();
                // foreach (Control ctrl in this.splitContainerTreeviewListbox.Panel2.Controls)
                // {

                // ctrl.Focus();

                // this.textBoxforFileDisplay.Text += ctrl.ToString();
                // this.textBoxforFileDisplay.Text += "\n";
                // this.textBoxforFileDisplay.Text += ctrl.Name;
                // this.textBoxforFileDisplay.Text += "\n";
                // this.textBoxforFileDisplay.Text += ctrl.Focused.ToString();
                // this.textBoxforFileDisplay.Text += "\n";
                // 
                // }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        #endregion

        #region Methods

        /// <summary>The on form closing.</summary>
        /// <param name="e">The e. </param>
        protected override void OnFormClosing(FormClosingEventArgs e)
        {
            base.OnFormClosing(e);

            Settings.Default.MainWindowLocationSetting = this.Location;
            Settings.Default.MainWindowSizeSetting = this.Size;
            Settings.Default.MainWindowWidthSetting = this.Width;
            Settings.Default.MainWindowHeightSetting = this.Height;
            Settings.Default.Save();

            // check for changes before exit
            if (this.savedFileContents != this.textBoxCurrentContents)
            {
                if (this.ConfirmContinueWithoutSaving())
                {
                    this.Dispose(true);
                    Application.Exit();
                }
                else
                {
                    e.Cancel = true;
                }
            }
            else
            {
                if (Settings.Default.ConfirmExitSetting.Equals(true))
                {
                    if (this.PreClosingConfirmation() == DialogResult.Yes)
                    {
                        this.Dispose(true);
                        Application.Exit();
                    }
                    else
                    {
                        e.Cancel = true;
                    }
                }
                else
                {
                    this.Dispose(true);
                    Application.Exit();
                }
            }
        }

        /// <summary>The create new file.</summary>
        /// <returns> The <see cref="string" /> File and path of new file </returns>
        private static string CreateNewFile()
        {
            string fileNameAndPathOfNewFile;
            using (var dialog = new SaveFileDialog())
            {
                dialog.Title = "Create New File";
                dialog.Filter = "txt files (*.txt)|*.txt|All files (*.*)|*.*";
                dialog.AddExtension = true;
                dialog.FilterIndex = 1;

                // dialog.RestoreDirectory = true;
                // dialog.InitialDirectory = this.treeView1.SelectedNode.FullPath;
                if (dialog.ShowDialog() == DialogResult.OK)
                {
                    FileOperations.FileWrite(dialog.FileName, string.Empty);
                }

                fileNameAndPathOfNewFile = dialog.FileName; // gets the full path of the new file
            }

            return fileNameAndPathOfNewFile;
        }

        /// <summary>The about snippet editor tool strip menu item click.</summary>
        /// <param name="sender">The sender. </param>
        /// <param name="e">The e. </param>
        private void AboutSnippetEditorToolStripMenuItemClick(object sender, EventArgs e)
        {
            using (new CenterWinDialog(this))
            {
                var formAbout = new FormAbout();
                formAbout.ShowDialog(this);
            }
        }
        /////// <summary>Allows fast jump to open the Change Log for testing purposes</summary>
        /////// <param name="sender">The sender. </param>
        /////// <param name="e">The e. </param>
        ////private void ChangelogForTestingToolStripMenuItemClick(object sender, EventArgs e)
        ////{
        ////    var changeLog = new FormChangeLog();
        ////    changeLog.Show();
        ////}

        /// <summary>Closes the file in terms of wiping text box, savedFileContents and path of open file.</summary>
        private void CloseFile()
        {
            this.textBoxforFileDisplay.Text = string.Empty;
            this.savedFileContents = string.Empty;
            this.fileAndPathOfOpenFile = string.Empty;
            this.UpdateFormElements();
        }

        /// <summary>Shows dialog to allow user to continue or return to file to save changes.</summary>
        /// <returns> The <see cref="bool" /> . </returns>
        private bool ConfirmContinueWithoutSaving()
        {
            return
                MessageBox.Show(
                    "Changes have been made to " + Path.GetFileName(this.fileAndPathOfOpenFile)
                    + " do you want to continue without saving changes?", 
                    "Discard Unsaved Changes?", 
                    MessageBoxButtons.YesNo) == DialogResult.Yes;
        }

        /// <summary>The delete tool strip menu item click.</summary>
        /// <param name="sender">The sender. </param>
        /// <param name="e">The e. </param>
        private void DeleteToolStripMenuItemClick(object sender, EventArgs e)
        {
            try
            {
                string fileToDelete = this.treeViewExplorer.SelectedNode.FullPath + "\\"
                                      + this.listBoxFileList.SelectedItem;
                FileOperations.FileDelete(fileToDelete);
                this.RefreshUi();
                this.listBoxFileList.SelectedIndex = 0;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        /// <summary>The exit click.</summary>
        /// <param name="sender">The sender. </param>
        /// <param name="e">The e. </param>
        private void ExitClick(object sender, EventArgs e)
        {
            Application.Exit();
        }

        /// <summary>The form settings form closed.</summary>
        /// <param name="sender">The sender. </param>
        /// <param name="e">The e. </param>
        private void FormSettingsFormClosed(object sender, FormClosedEventArgs e)
        {
            this.RefreshUi();
        }

        /// <summary>The list box 1 selected index changed.</summary>
        /// <param name="sender">The sender. </param>
        /// <param name="e">The e. </param>
        private void ListBox1SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                // if there is a list box item selected and the text box text is not equal to the savedfile contents, and the list box item selected isn't the same as the file that is open
                if ((!this.listBoxFileList.SelectedIndex.Equals(-1))
                    && (this.textBoxCurrentContents != this.savedFileContents)

                    /*&& (this.listBoxFileList.SelectedItem.ToString() != Path.GetFileName(this.fileAndPathOfOpenFile))*/)
                {
                    // ask to save changes to file
                    if (this.listBoxFirstPass)
                    {
                        this.listBoxFirstPass = false;
                        if (this.isOnRefresh == false && this.ConfirmContinueWithoutSaving())
                        {
                            this.listBoxFirstPass = false;
                            this.CloseFile();
                            this.UpdateTextBoxWithNewFile();
                        }
                        else
                        {
                            this.UpdateTextBoxWithUnsavedFile();
                        }
                    }
                    else
                    {
                        this.listBoxFirstPass = true;
                    }
                }
                else
                {
                    this.CloseFile();
                    this.UpdateTextBoxWithNewFile();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        /// <summary>Updates the list box with files.</summary>
        /// <param name="e">The e. </param>
        private void ListBoxAddFiles(TreeViewEventArgs e)
        {
            e.Node.BackColor = SystemColors.Highlight;
            e.Node.ForeColor = SystemColors.HighlightText;

            this.lastSelectedNode = e.Node;
            this.lastSelectedNodeFullPath = e.Node.FullPath;
            this.listBoxFileList.Items.Clear();
            this.treeViewFirstPass = true;

            if (e.Node.FullPath.Length <= 0)
            {
                return;
            }

            IEnumerable<string> childrenFiles = FileOperations.GetFiles(e.Node.FullPath);

            var fileExtensions =
                Settings.Default.FileExtensionsPreferencesSetting.Deserialize<Dictionary<string, bool>>();
            var fileExtensionsChosen = new ArrayList();

            // add chosen extensions to a new arraylist
            foreach (KeyValuePair<string, bool> keyValuePair in fileExtensions)
            {
                if (keyValuePair.Value)
                {
                    fileExtensionsChosen.Add(keyValuePair.Key);
                }
            }

            foreach (string str in childrenFiles)
            {
                string extension = Path.GetExtension(str);
                if (extension != null && fileExtensionsChosen.Contains(extension))
                {
                    // check if sub list contains the extension variable, if so add the file to the list
                    // is there a better way to do this?
                    // ReSharper disable AssignNullToNotNullAttribute
                    this.listBoxFileList.Items.Add(Path.GetFileName(str));

                    // ReSharper restore AssignNullToNotNullAttribute
                }
            }
        }

        /// <summary>Moves clockwise to the next control: TreeView, ListBox, Textbox</summary>
        /// <param name="sender">The sender. </param>
        /// <param name="e">The e. </param>
        private void MoveToNextControlToolStripMenuItemClick(object sender, EventArgs e)
        {
            // seems inelegant, but it's simple and it works. would like a list of the controls instead? or overkill? this does work perfectly.
            if (this.treeViewExplorer.Focused)
            {
                this.listBoxFileList.Focus();
            }
            else if (this.listBoxFileList.Focused)
            {
                this.textBoxforFileDisplay.Focus();
            }
            else if (this.textBoxforFileDisplay.Focused)
            {
                this.treeViewExplorer.Focus();
            }
        }

        /// <summary>The new tool strip menu item click.</summary>
        /// <param name="sender">The sender. </param>
        /// <param name="e">The e. </param>
        private void NewToolStripMenuItemClick(object sender, EventArgs e)
        {
            try
            {
                if (this.savedFileContents != this.textBoxCurrentContents)
                {
                    if (this.ConfirmContinueWithoutSaving())
                    {
                        this.CloseFile();
                        string fileNameAndPathOfNewFile = CreateNewFile();
                        this.lastSelectedNodeFullPath = Path.GetFullPath(fileNameAndPathOfNewFile);
                        this.RefreshUi();
                        this.listBoxFileList.SelectedItem = Path.GetFileName(fileNameAndPathOfNewFile);
                    }
                }
                else
                {
                    this.CloseFile();
                    string fileNameAndPathOfNewFile = CreateNewFile();
                    this.lastSelectedNodeFullPath = Path.GetFullPath(fileNameAndPathOfNewFile);
                    this.RefreshUi();
                    this.listBoxFileList.SelectedItem = Path.GetFileName(fileNameAndPathOfNewFile);
                }

                // TODO add the selected treeview to the save as? tried this with initial directory property of the dialog box, not working though. 
                // put contents of ifs into another method to remove duplication
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        /// <summary>The options tool strip menu item click.</summary>
        /// <param name="sender">The sender. </param>
        /// <param name="e">The e. </param>
        private void OptionsToolStripMenuItemClick(object sender, EventArgs e)
        {
            var formSettings = new FormSettings();
            formSettings.FormClosed += this.FormSettingsFormClosed;
            formSettings.ShowDialog(this);
        }

        /// <summary>
        ///   The pre closing confirmation.
        /// </summary>
        /// <returns> The <see cref="DialogResult" /> . </returns>
        private DialogResult PreClosingConfirmation()
        {
            DialogResult res = MessageBox.Show(
                "Are you sure you want to exit?", "Confirm exit", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            return res;
        }

        /// <summary>The refresh tool strip menu item click.</summary>
        /// <param name="sender">The sender. </param>
        /// <param name="e">The e. </param>
        private void RefreshToolStripMenuItemClick(object sender, EventArgs e)
        {
            this.RefreshUi();
        }

        /// <summary>The refresh tree view.</summary>
        /// <param name="fullPathToFind">The full Path To Find. </param>
        private void RefreshTreeView(string fullPathToFind = "")
        {
            string[] filePathElements = fullPathToFind.Length == 0
                                            ? this.lastSelectedNodeFullPath.Split('\\')
                                            : fullPathToFind.Split('\\');

            bool lastNodeWasExpanded = this.lastSelectedNode.IsExpanded;
            this.treeViewExplorer.Nodes.Clear();
            this.TreeviewPopulate();
            TreeNodeCollection nodes = this.treeViewExplorer.Nodes;
            foreach (TreeNode node in nodes)
            {
                this.TreeviewFindNode(node, filePathElements);
            }

            if (!lastNodeWasExpanded && (this.treeViewExplorer.SelectedNode != null))
            {
                this.treeViewExplorer.SelectedNode.Collapse();
            }
        }

        /// <summary>
        ///   Refreshes the whole UI.
        /// </summary>
        private void RefreshUi()
        {
            this.isOnRefresh = true;
            this.RefreshTreeView(); // also refreshes the listbox on treviewafterselect event
            this.listBoxFileList.SelectedItem = this.lastSelectedListBoxItem;
            this.isOnRefresh = false;

            // this.textBoxforFileDisplay.Text = this.textBoxCurrentContents;
        }

        /// <summary>The save as tool strip menu item click.</summary>
        /// <param name="sender">The sender. </param>
        /// <param name="e">The e. </param>
        private void SaveAsToolStripMenuItemClick(object sender, EventArgs e)
        {
            try
            {
                string fileThatWasSaved = FileOperations.SaveFileAs(
                    this.textBoxforFileDisplay.Text, this.treeViewExplorer.SelectedNode.FullPath);
                this.savedFileContents = this.textBoxforFileDisplay.Text;
                this.RefreshTreeView(Path.GetFullPath(fileThatWasSaved));
                this.listBoxFileList.SelectedItem = Path.GetFileName(fileThatWasSaved);
                this.UpdateTextBoxWithNewFile(fileThatWasSaved);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        /// <summary>The save click.</summary>
        /// <param name="sender">The sender. </param>
        /// <param name="e">The e. </param>
        private void SaveClick(object sender, EventArgs e)
        {
            try
            {
                if (File.Exists(this.fileAndPathOfOpenFile))
                {
                    FileOperations.FileWrite(this.fileAndPathOfOpenFile, this.textBoxforFileDisplay.Text);
                    this.savedFileContents = this.textBoxforFileDisplay.Text;
                    this.toolStripStatusLabel2.Text = "last saved at " + DateTime.Now.ToShortTimeString();
                }
                else
                {
                    this.SaveAsToolStripMenuItemClick(sender, e);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        /// <summary>The text box 1 text changed.</summary>
        /// <param name="sender">The sender. </param>
        /// <param name="e">The e. </param>
        private void TextBox1TextChanged(object sender, EventArgs e)
        {
            this.textBoxCurrentContents = this.textBoxforFileDisplay.Text;
            Debug.WriteLine(this.textBoxforFileDisplay.Text);
        }

        /// <summary>The tree view 1_ after expand.</summary>
        /// <param name="sender">The sender. </param>
        /// <param name="e">The e. </param>
        private void TreeView1AfterExpand(object sender, TreeViewEventArgs e)
        {
            // this.TreeView1AfterSelect(sender, e);
        }

        /// <summary>The tree view 1 after select.</summary>
        /// <param name="sender">The sender. </param>
        /// <param name="e">The e. </param>
        private void TreeView1AfterSelect(object sender, TreeViewEventArgs e)
        {
            try
            {
                string fullPath = e.Node.FullPath;

                // if the text in the text bx is different from the saved file and the selected node is different from the last nide and this is the fist time though continue.
                if ((this.textBoxforFileDisplay.Text != this.savedFileContents)
                    && (fullPath != this.lastSelectedNode.ToString()) && this.treeViewFirstPass)
                {
                    if (this.isOnRefresh == false && this.ConfirmContinueWithoutSaving())
                    {
                        this.CloseFile();
                        this.ListBoxAddFiles(e);
                    }
                    else
                    {
                        if (this.treeViewFirstPass)
                        {
                            this.treeViewFirstPass = false;
                            this.treeViewExplorer.SelectedNode = this.lastSelectedNode;
                            this.UpdateTextBoxWithUnsavedFile();
                        }
                        else
                        {
                            // reset the counter/ bool variable to stop the double pop up on file change
                            // as the file changing triggers listboxselecteditemchanged
                            // seems like a messy soloutoin, but it could do the job, well it does, but it coule be better, think on this one. 
                            this.treeViewFirstPass = true;
                        }
                    }
                }
                else
                {
                    this.ListBoxAddFiles(e);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        /// <summary>The tree view 1 before expand.</summary>
        /// <param name="sender">The sender. </param>
        /// <param name="e">The e. </param>
        private void TreeView1BeforeExpand(object sender, TreeViewCancelEventArgs e)
        {
            if (e.Node.Nodes[0].Text != "*")
            {
                return;
            }

            e.Node.Nodes.Clear();
            FileOperations.FillChildNodes(e.Node);
        }

        /// <summary>The tree view 1 before select.</summary>
        /// <param name="sender">The sender. </param>
        /// <param name="e">The e. </param>
        private void TreeView1BeforeSelect(object sender, TreeViewCancelEventArgs e)
        {
            if (this.treeViewExplorer.SelectedNode == null)
            {
                return;
            }

            this.treeViewExplorer.SelectedNode.BackColor = SystemColors.ControlLightLight; // SystemColors.Control;
            this.treeViewExplorer.SelectedNode.ForeColor = SystemColors.ControlText;
        }

        /// <summary>The tree view 1 node mouse click.</summary>
        /// <param name="sender">The sender. </param>
        /// <param name="e">The e. </param>
        private void TreeView1NodeMouseClick(object sender, TreeNodeMouseClickEventArgs e)
        {
            if (this.treeViewExplorer.SelectedNode == e.Node)
            {
                this.TreeView1AfterSelect(sender, new TreeViewEventArgs(e.Node));
            }
        }

        /// <summary>The find node.</summary>
        /// <param name="treeNode">The tree node. </param>
        /// <param name="fileNamesToFind">The file Names To Find. </param>
        private void TreeviewFindNode(TreeNode treeNode, string[] fileNamesToFind)
        {
            foreach (TreeNode tn in treeNode.Nodes)
            {
                foreach (string str in fileNamesToFind)
                {
                    if (tn.Text == str)
                    {
                        this.treeViewExplorer.SelectedNode = tn;
                        this.treeViewExplorer.SelectedNode.Expand();
                        break;
                    }
                }

                this.TreeviewFindNode(tn, fileNamesToFind);
            }
        }

        /// <summary>
        ///   The populates the TreeView.
        /// </summary>
        private void TreeviewPopulate()
        {
            // this.treeView1.Nodes.Clear();
            string[] localDrives = Environment.GetLogicalDrives();
            foreach (string str in localDrives)
            {
                var tn = new TreeNode(str);
                this.treeViewExplorer.Nodes.Add(tn);
                FileOperations.FillChildNodes(tn);
            }
        }

        /// <summary>
        ///   The update form elements.
        /// </summary>
        private void UpdateFormElements()
        {
            if (this.fileAndPathOfOpenFile.Length == 0)
            {
                this.Text = "Snippet Editor";
                this.toolStripStatusLabel1.Text = string.Empty;
            }
            else
            {
                this.Text = "Snippet Editor | " + Path.GetFileName(this.fileAndPathOfOpenFile);
                this.toolStripStatusLabel1.Text = "Current file: " + this.fileAndPathOfOpenFile;
            }
        }

        /// <summary>Updates the textbox with the file read from the passed in file path, or creates a file path from the 
        ///   selected TreeView node and ListBox item and opens that if no path is passed in.</summary>
        /// <param name="fullPathOfFileToOpen">Optional. The full path of the file to open. </param>
        private void UpdateTextBoxWithNewFile(string fullPathOfFileToOpen = "")
        {
            this.toolStripStatusLabel2.Text = string.Empty;

            if (fullPathOfFileToOpen.Length == 0)
            {
                // create the file path that is to be opened
                fullPathOfFileToOpen = this.treeViewExplorer.SelectedNode.FullPath + "\\"
                                       + this.listBoxFileList.SelectedItem;
            }

            this.textBoxforFileDisplay.Text = FileOperations.FileRead(fullPathOfFileToOpen);
            this.savedFileContents = this.textBoxforFileDisplay.Text;
            if (!this.listBoxFileList.SelectedIndex.Equals(-1))
            {
                this.lastSelectedListBoxItem = this.listBoxFileList.SelectedItem.ToString();
            }
            
            this.fileAndPathOfOpenFile = fullPathOfFileToOpen;
            this.UpdateFormElements();
        }

        /// <summary>The update text box with unsaved file.</summary>
        private void UpdateTextBoxWithUnsavedFile()
        {
            string tempChangesHolder = this.textBoxCurrentContents;
            this.treeViewExplorer.SelectedNode = new TreeNode(Path.GetDirectoryName(this.fileAndPathOfOpenFile));
            this.listBoxFileList.SelectedItem = this.lastSelectedListBoxItem;
            this.textBoxforFileDisplay.Text = tempChangesHolder;
        }

        #endregion

        /// <summary>The open tool strip menu item click.</summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The e.</param>
        private void OpenToolStripMenuItemClick(object sender, EventArgs e)
        {
            
            string fileToOpen;
            using (new CenterWinDialog(this))
            {
                var openDialog = new OpenFileDialog();
                openDialog.ShowDialog(this);

                fileToOpen = openDialog.FileName;
            }
            this.RefreshTreeView(fileToOpen);
            this.UpdateTextBoxWithNewFile(fileToOpen);
            
            
        }
    }
}