﻿namespace SnippetsWinformsUI
{
    using System;
    using System.IO;
    using System.Windows.Forms;

    /// <summary>
    /// Description of ChangeLog.
    /// </summary>
    public partial class FormChangeLog : Form
    {
        #region Constructors and Destructors

        /// <summary>Initialises a new instance of the <see cref="FormChangeLog"/> class.</summary>
        public FormChangeLog()
        {
            // The InitializeComponent() call is required for Windows Forms designer support.
            this.InitializeComponent();

            // textboxChangeLog.Text = FileOperations.FileRead("../../ChangeLog.txt");	
            var source = new FileStream("../../ChangeLog.html", FileMode.Open, FileAccess.Read);
            this.webBrowser1.DocumentStream = source;
        }

        #endregion

        /// <summary> The button close click. </summary>
        /// <param name="sender"> The sender. </param>
        /// <param name="e"> The e. </param>
        private void ButtonCloseClick(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}