﻿namespace SnippetsWinformsUI
{
    using System;
    using System.Collections.Generic;
    using System.Windows.Forms;
    /* ========== UnSupported FileTypes ===========
     * .rtf
     */

    /// <summary>
    ///   The form file extensions.
    /// </summary>
    public partial class FormFileExtensions : Form
    {
        #region Fields

        /// <summary>
        ///   True if one or more of the check box list items are checked.
        /// </summary>
        private bool allChecked;

        /// <summary>
        ///   Holds file extensions and whether they are selected or not.
        /// </summary>
        private Dictionary<string, bool> fileExtensions = new Dictionary<string, bool>();

        /// <summary>
        ///   Holds the file extensions preferences setting.
        /// </summary>
        private string serialized;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        ///   Initialises a new instance of the <see cref="FormFileExtensions" /> class.
        /// </summary>
        public FormFileExtensions()
        {
            this.InitializeComponent();
            this.serialized = Settings.Default.FileExtensionsPreferencesSetting;
        }

        #endregion

        #region Methods

        /// <summary>The button cancel click.</summary>
        /// <param name="sender">The sender. </param>
        /// <param name="e">The e. </param>
        private void BtnCancelClick(object sender, EventArgs e)
        {
            this.Close();
        }

        /// <summary>The button save click.</summary>
        /// <param name="sender">The sender. </param>
        /// <param name="e">The e. </param>
        private void BtnSaveClick(object sender, EventArgs e)
        {
            this.fileExtensions.Clear();
            bool somethingIsChecked = false;
            for (int i = 0; i < this.checkedListBox1.Items.Count; i++)
            {
                this.fileExtensions.Add(
                    this.checkedListBox1.Items[i].ToString(), this.checkedListBox1.GetItemChecked(i));
                if (this.checkedListBox1.GetItemChecked(i))
                {
                    somethingIsChecked = true;
                }
            }

            // If no boxes have been ticked on save this sets .txt to true so that it finds some files
            if (!somethingIsChecked)
            {
                this.fileExtensions[".txt"] = true;
            }

            this.serialized = this.fileExtensions.Serialize();
            Settings.Default.FileExtensionsPreferencesSetting = this.serialized;
            Settings.Default.Save();
            Console.WriteLine(this.serialized);
            this.Close();
        }

        /// <summary>Checks all tick boxes in the ListView.</summary>
        /// <param name="sender">The sender. </param>
        /// <param name="e">The e. </param>
        private void BtnSelectAllClick(object sender, EventArgs e)
        {
            if (!this.allChecked)
            {
                // if the allchecked variable is false then tick all boxes
                for (int i = 0; i < this.checkedListBox1.Items.Count; i++)
                {
                    this.checkedListBox1.SetItemChecked(i, true);
                }

                this.allChecked = true;
            }
            else
            {
                // if the allchecked variable is true then untick all boxes
                for (int i = 0; i < this.checkedListBox1.Items.Count; i++)
                {
                    this.checkedListBox1.SetItemChecked(i, false);
                }

                this.allChecked = false;
            }
        }

        /// <summary>The form file extensions load.</summary>
        /// <param name="sender">The sender. </param>
        /// <param name="e">The e. </param>
        private void FormFileExtensionsLoad(object sender, EventArgs e)
        {
             

            // the line below is used to set the check boxes with new etensions the firt time, once saved it works, not sure why, need to look into this as it's a horrid work around!
             // this.serialized = "<ArrayOfKeyValueOfstringboolean xmlns:i=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns=\"http://schemas.microsoft.com/2003/10/Serialization/Arrays\"><KeyValueOfstringboolean><Key>.aspx</Key><Value>false</Value></KeyValueOfstringboolean><KeyValueOfstringboolean><Key>.cs</Key><Value>false</Value></KeyValueOfstringboolean><KeyValueOfstringboolean><Key>.txt</Key><Value>true</Value></KeyValueOfstringboolean><KeyValueOfstringboolean><Key>.xml</Key><Value>false</Value></KeyValueOfstringboolean><KeyValueOfstringboolean><Key>.vb</Key><Value>false</Value></KeyValueOfstringboolean><KeyValueOfstringboolean><Key>.css</Key><Value>false</Value></KeyValueOfstringboolean><KeyValueOfstringboolean><Key>.html</Key><Value>false</Value></KeyValueOfstringboolean><KeyValueOfstringboolean><Key>.resx</Key><Value>false</Value></KeyValueOfstringboolean><KeyValueOfstringboolean><Key>.config</Key><Value>false</Value></KeyValueOfstringboolean></ArrayOfKeyValueOfstringboolean>";
            this.serialized = Settings.Default.FileExtensionsPreferencesSetting;

            this.fileExtensions = this.serialized.Deserialize<Dictionary<string, bool>>();
            this.checkedListBox1.Items.Clear();

            foreach (KeyValuePair<string, bool> keyValuePair in this.fileExtensions)
            {
                this.checkedListBox1.Items.Add(keyValuePair.Key, keyValuePair.Value);
            }
        }

        #endregion

        // dictionary to serialize to string
        // Dictionary<string, object> myDict = new Dictionary<string, object>();
        // // add items to the dictionary...
        // myDict.Add(...);
        // // serialization is straight-forward
        // string serialized = fileExtensions.Serialize();
        // ...
        // // deserialization is just as simple
        // Dictionary<string, object> myDictCopy = serialized.Deserialize<Dictionary<string,object>>();
    }
}